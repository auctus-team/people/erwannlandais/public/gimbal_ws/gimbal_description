# Gimbal description

URDF description of different types of gimbal (with a big or small vial, with the Optitrack object for HandEye calibration, ...). 
The URDF are obtained thanks to the onshape-to-robot package : https://github.com/Rhoban/onshape-to-robot

## Classic case (gte / pte)

Procedure when new file : 

* Change <mesh filename="package:///
* Change <limit effort="1" velocity="20" to :

  * Joint 1 (= base) : <limit effort="0.5" velocity="5.28"
  * Joint 2 : <limit effort="0.5" velocity="5.61"  
  * Joint 3 (= EE) : <limit effort="0.5" velocity="5.67"

* Add to robot.urdf : 
    * mid_vial link and joint concerned
    * world link and joint concerned

    <link name="world"/>

    <joint name="world_to_base" type="fixed">
      <origin rpy="-1.57078 0 0" xyz="0 0 0"/> 
      <parent link="world"/>
      <child link="base"/>
    </joint>



* Create gimbal.urdf.xacro, by remplacing : 

<robot name="onshape">


    <link name="world"/>

    <joint name="world_to_base" type="fixed">
      <origin rpy="-1.57078 0 0" xyz="0 0 0"/> 
      <parent link="world"/>
      <child link="base"/>
    </joint>

[...]

</robot>

==> 

<robot xmlns:xacro="http://www.ros.org/wiki/xacro" name="gimbal">
  <!-- <xacro:macro name="gimbal" params="connected_to:='world' xyz:='0 0 0' rpy:='-1.57078 0 0'"> -->


    <!-- <link name="world"/>

    <joint name="world_to_base" type="fixed">
      <origin rpy="-1.57078 0.0 0.0" xyz="0 0 0"/> 
      <parent link="world"/>
      <child link="base"/>
    </joint> -->
  <xacro:arg name="connected_to" default="world" />    
  <xacro:arg name="xyz" default="0 0 0" />
  <xacro:arg name="rpy" default="-1.57078 0 0" />


    <link name="$(arg connected_to)"/>

    <joint name="$(arg connected_to)_to_base" type="fixed">
      <origin rpy="$(arg rpy)" xyz="$(arg xyz)"/> 
      <parent link="$(arg connected_to)"/>
      <child link="base"/>
    </joint>

[...]

 <xacro:include filename="$(find gimbal_description)/robots/gimbal.gazebo" />

</robot>

## Calib case

Procedure when new file : 

* Change <mesh filename="package:///"
* Change <limit effort="0.5" velocity="6"/>


<joint name="mid_vial_to_vial_eq_panda" type="fixed">
  <origin xyz="0 0 -27e-3" rpy="-3.141592653589793116 0 0" />
  <parent link="hn05_n102_arm2_3" />
  <child link="mid_vial" />

</joint>